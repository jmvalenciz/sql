use PDF;

insert into jugadores values(123,'Juan Gomez','Delantero','1987/02/10');
insert into jugadores values(456,'David Perez','Delantero','1999/06/4');
insert into jugadores values(345,'Carlos Lopez','Arquero','1997/07/1');
insert into jugadores values(789,'Esteban Ramirez','Defensa','1999/06/4');
insert into jugadores values(652,'Daniel Holguin','Defensa','2000/06/25');
insert into jugadores values(632,'Javier Ortiz','Volante','2000/07/12');
insert into jugadores values(854,'Camilo Upegui','Volante','1995/07/5');
insert into jugadores values(145,'Daniel Sanchez','Arquero','1989/12/6');
insert into jugadores values(951,'Marcelo Cepeda','Defensa','1982/01/30');

insert into estadios values('1A','Atanasio',55000,'Medellin');
insert into estadios values('1B','El campin',56000,'Bogota');
insert into estadios values('3D','Guerrero',4000,'Cali');
insert into estadios values('4G','Metropolitano',11000,'Barranquilla');

insert into equipos values('AA','Medellin','1997','1A','Medellin');
insert into equipos values('AB','Nacional','1913','1A','Medellin');
insert into equipos values('AC','Millonarios','1956','1B','Bogota');
insert into equipos values('DE','Cali','1975','3D','Cali');
insert into equipos values('EA','Junior','1987','4G','Barranquilla');
insert into equipos values('FF','Envigado','1965','1A','Envigado');

insert into partidos values(1,'2017/02/1','AA','AB','1A',1,1);
insert into partidos values(2,'2017/02/1','AC','DE','1B',1,0);
insert into partidos values(3,'2017/02/15','EA','FF','1A',0,0);
insert into partidos values(4,'2017/02/28','FF','AA','1A',1,2);
insert into partidos values(5,'2017/05/15','DE','FF','3D',1,0);

insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
values('0:12:00',1,1,0,123,'gol en el minuto 12');
insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
 values('0:45:00',1,0,1,456,'gol en el minuto 45');
insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
 values('0:45:00',2,0,1,345,'gol en el minuto 45');
insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
 values('1:00:00',4,1,0,145,'gol en el minuto 60');
insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
 values('1:29:00',4,1,0,123,'gol en el minuto 89');
insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
 values('1:30:00',4,1,0,123,'gol en el minuto 90');
insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion) 
 values('0:56:00',5,0,1,789,'gol en el minuto 56');

insert into jugadoresxequipos values(123,'AA','2016/1/1',null);
insert into jugadoresxequipos values(456,'AB','2015/7/1','2015/12/31');
insert into jugadoresxequipos values(456,'AB','2017/1/1',null);
insert into jugadoresxequipos values(345,'AC','2017/3/1',null);
insert into jugadoresxequipos values(789,'DE','2017/3/4',null);
insert into jugadoresxequipos values(854,'EA','2017/6/5',null);
insert into jugadoresxequipos values(145,'FF','2016/2/1',null);
insert into jugadoresxequipos values(951,'AA','2013/5/6','2014/05/6');
insert into jugadoresxequipos values(951,'DE','2015/6/6',null);
insert into jugadoresxequipos values(652,'AA','2014/8/16','2016/11/30');
insert into jugadoresxequipos values(652,'AB','2017/7/11',null);

