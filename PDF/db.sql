create database PDF;
use PDF;

create table jugadores(
	id_jugador int,
    Nombre varchar(40) not null,
    posicion varchar(12),
    fecha_nto date NOT NULL,
    primary key(id_jugador)
);

create table estadios(
	id_estadio char(2),
    nombre varchar(30) not null,
    capacidad int not null,
    ciudad varchar(30) not null,
    primary key(id_estadio)
);

create table equipos(
	id_equipo char(2),
    nombre varchar(20) not null,
    año_fundacion year(4) not null,
    id_estadio char(2) not null,
    ciudad varchar(30) not null,
    primary key(id_equipo),
	foreign key(id_estadio) references estadios(id_estadio)
);

create table partidos(
	id_partido int,
    fecha date NOT NULL,
    id_equipo_casa char(2) NOT NULL,
    id_equipo_vist char(2) not null,
    id_estadio char(2) not null,
goles_casa int not null,
    goles_visit int not null,
    primary key(id_partido),
	foreign key(id_equipo_casa) references equipos(id_equipo),
    foreign key(id_equipo_vist) references equipos(id_equipo),
    foreign key(id_estadio) references estadios(id_estadio)
);

create table goles(
	id_gol int not null auto_increment,
	minuto time not null,
    id_partido int not null,
	 gol_casa boolean not null,
	 gol_visit boolean not null,
    id_jugador int not null,
    descripcion varchar(50),
    primary key(id_gol),
    foreign key(id_partido) references partidos(id_partido),
	 foreign key(id_jugador) references jugadores(id_jugador)

);

create table reg_goles (
   id int not null auto_increment,
   usuario varchar(20) not null,
   cambio char(6) not null,
   fecha datetime not null,
   minuto_gol time not null,
   id_gol int not null,
   id_partido_gol int not null,
   id_jugador_gol int not null,
   descripcion varchar(30),
   primary key(id)
);

create table jugadoresxequipos(
	id_jugador int,
    id_equipo char(2),
    fecha_inicio date not null,
    fecha_fin date,
    primary key(id_jugador,id_equipo,fecha_inicio),
    foreign key(id_jugador) references jugadores(id_jugador),
    foreign key(id_equipo) references equipos(id_equipo)
);

