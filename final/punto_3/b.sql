use PDF;


create trigger Actualizar_marcador 
AFTER INSERT on goles 
for each row 
	UPDATE partidos p
	SET	goles_casa=goles_casa + new.gol_casa ,
			goles_visit= goles_visit+ new.gol_casa
	WHERE new.id_partido=p.id_partido;

insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion)
values('0:27:00',1,0,1,456,'gol en el minuto 12'); -- gol casa en el partido 1

insert into goles(minuto,id_partido,gol_casa,gol_visit,id_jugador,descripcion)
values('0:27:00',2,0,1,456,'gol en el minuto 12'); -- gol visitante en el partido 2
