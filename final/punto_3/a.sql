use PDF;

-- ----------------------------------------------------------------------

create trigger reg_up_gol
after UPDATE on goles
for each row
	insert into reg_goles(id_gol,usuario,cambio,fecha,minuto_gol,id_partido_gol,id_jugador_gol,descripcion)
	values(new.id_gol,USER(),'UPDATE',NOW(),old.minuto,old.id_partido,old.id_jugador,old.descripcion);

create trigger reg_del_gol
before DELETE on goles
for each row
	insert into reg_goles(id,usuario,cambio,fecha,minuto_gol,id_partido_gol,id_jugador_gol,descripcion)
	values(old.id_gol,USER(),'DELETE',NOW(),old.minuto,old.id_partido,old.id_jugador,old.descripcion);

