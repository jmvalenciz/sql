use PDF;
drop table if exists topJugadores;
create table topJugadores(
   posicion int not null auto_increment,
   id_jugador int not null,
   primary key(posicion),
   foreign key(id_jugador) references jugadores(id_jugador)
);

-- ------------------------------------------------------------
drop procedure if exists top_jugadores;
DELIMITER &&
create procedure top_jugadores()
begin
   declare jugador, goles int;
   declare fin boolean default false;
   declare c1 cursor for
   select id_jugador, count(id_jugador)
   from goles
   group by id_jugador;
   declare continue handler
   for not found set fin = true;
   open c1;
   c1_loop: loop
      fetch c1 into jugador,goles;
      if fin then
         close c1;
         leave c1_loop;
      end if;
      insert into topJugadores(id_jugador) values(jugador);
   end loop c1_loop;
   select * from topJugadores;
end;
&&
call top_jugadores();
