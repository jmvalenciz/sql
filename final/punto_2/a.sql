use PDF;
 
 drop table IF exists topEquipos;
 create table topEquipos(
    posicion int AUTO_INCREMENT not null,
     id_equipo char(2) not null,
     primary key(posicion)
 );
 
    drop table if exists resultadosPartidos;
    create table  resultadosPartidos(id_equipo char(2), id_partido int, puntuacion int);
    insert into resultadosPartidos
    select id_equipo_casa, id_partido,
      IF(goles_casa>goles_visit,
         3,
         IF(goles_casa=goles_visit,1,0))
    from partidos p;
    insert into resultadosPartidos
    select id_equipo_vist, id_partido,
      IF(goles_visit>goles_casa,
         3,
         IF(goles_casa=goles_visit,1,0))
    from partidos p;

drop procedure if exists top_equipos;
delimiter ;;
 create procedure top_equipos()
 begin
    declare equipo char(2);
    declare puntos int;
    declare fin boolean default false;
    
    declare c1 cursor for
    select id_equipo, sum(puntos)
    from resultadosPartidos
    group by id_equipo
    order by sum(puntos)
    desc;
    declare continue handler
    for not found set fin=true;
    
    open c1;
    
    c1_loop: loop
       fetch c1 into equipo,puntos;
       if fin then
          close c1;
          leave c1_loop;
       end if;
       insert into topEquipos(id_equipo) values(equipo);
    end loop c1_loop;
 end;;
 call top_equipos();

select * from topEquipos;
