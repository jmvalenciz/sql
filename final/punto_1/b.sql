use PDF;

DELIMITER $$
DROP function IF EXISTS tiempoEnEquipo;

create FUNCTION tiempoEnEquipo(jugador int, equipo char(2))
RETURNS varchar(30)
DETERMINISTIC
BEGIN

	declare tiempo int default 0;
	declare fin boolean default false; 
	declare inicio, final date;

	declare c1 cursor for 
	select fecha_inicio, fecha_fin
	from jugadoresxequipos je
	where id_jugador= jugador and id_equipo= equipo;

	declare continue handler 
	for not found set fin= true;

	open c1;

	c1_loop : loop

		fetch c1 into inicio,final;
		if fin then
			close c1;
			leave c1_loop;
		end if;
		set final = IF(final IS NULL,NOW(),final);
		set tiempo = tiempo + ABS(DATEDIFF(inicio,final));

	end loop c1_loop;

	return tiempo;


END;
$$
select tiempoEnEquipo(456,'AB');

