use PDF;

DROP function IF EXISTS datosPartido;

DELIMITER $$

create FUNCTION datosPartido(idPartido int) 
RETURNS varchar(42) DETERMINISTIC
BEGIN

	declare e_casa varchar(20);
	declare e_visit varchar(20);
	declare g_casa, g_visit smallint;

	SELECT id_equipo_casa, id_equipo_visit, goles_casa,goles_visit 
	INTO e_casa, e_visit, g_casa,g_visit
   FROM partidos
   where id_partido=idPartido;

	SELECT nombre
	INTO e_casa
	FROM equipos
	WHERE id_equipo = e_casa;

	SELECT nombre
	INTO e_visit
	FROM equipos
	WHERE id_equipo = e_visit;


   return CONCAT(e_casa,'(',g_casa,')','-',e_visit,'(',g_visit,')');	

END;

$$
select datosPartido(3);

