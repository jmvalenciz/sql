use PDF;

DROP function IF EXISTS estadioMasUsado;
DELIMITER $$
create FUNCTION estadioMasUsado(jugador int)
RETURNS varchar(20)
DETERMINISTIC
BEGIN

	declare estadio varchar(20);
	
	create temporary table jugadoresxpartidos
	select j.id_jugador, p.id_partido
	from jugadores j, partidos p, jugadoresxequipos je
	where j.id_jugador= je.id_jugador and (
			je.id_equipo=p.id_equipo_casa or 
			je.id_equipo=p.id_equipo_visit
	);

	select p.id_estadio
	into estadio
	from jugadoresxpartidos jp, partidos p
	where jp.id_jugador=jugador and p.id_partido = jp.id_partido
	group by p.id_estadio
	order by jp.id_jugador
	limit 1;

	/*select nombre
	into estadio
	from estadios e
	where estadio = id_estadio;
*/
	return estadio;

END;
$$
select estadioMasUsado(456);

